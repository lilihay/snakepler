import time

import pygame
import random
import math

HIGHT = 500
WIDTH = 700
SPEED = 3
BODY_W = 15
FRUIT_W = 10
pos = []
clock = pygame.time.Clock()
screen = pygame.display.set_mode((WIDTH, HIGHT))

# screen
game_window = pygame.display.set_mode((WIDTH, HIGHT))

# colors
black = pygame.Color(0, 0, 0)
white = pygame.Color(255, 255, 255)
red = pygame.Color(255, 0, 0)
green = pygame.Color(0, 255, 0)
blue = pygame.Color(0, 0, 255)


def snake():
    """
    snake game, here is the main function of the game
    :return: none
    """
    pygame.init()

    # snake build
    snakeY = 150
    snakeX = 150
    snake_speedX = SPEED
    snake_speedY = 0
    direction = "right"
    snake_width = 15
    snake_length = 5

    game_over = False

    # fruit build
    fruitX = random.randrange(1, WIDTH)
    fruitY = random.randrange(1, HIGHT)
    running = True
    # main loop
    while running:

        screen.fill((0, 0, 0))
        snakeX += snake_speedX
        snakeY += snake_speedY

        snake_head = pygame.Rect(snakeX, snakeY, snake_width, snake_width)
        pygame.draw.rect(screen, red, snake_head, 0, 10)
        # for loop that gets player input
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False

            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_RIGHT and direction != "left" and snake_speedX == 0:
                    snake_speedX += SPEED
                    snake_speedY = 0
                    direction = "right"
                elif event.key == pygame.K_LEFT and direction != "right" and snake_speedX == 0:
                    snake_speedX -= SPEED
                    snake_speedY = 0
                    direction = "left"
                elif event.key == pygame.K_DOWN and direction != "up" and snake_speedY == 0:
                    snake_speedY += SPEED
                    direction = "down"
                    snake_speedX = 0
                elif event.key == pygame.K_UP and direction != "down" and snake_speedY == 0:
                    snake_speedY -= SPEED
                    snake_speedX = 0
                    direction = "up"
        # keeps snake in frame
        if snakeX > WIDTH:
            snakeX = 0
        if snakeY > HIGHT:
            snakeY = 0
        if snakeX < 0:
            snakeX = WIDTH
        if snakeY < 0:
            snakeY = HIGHT
        # drawing first fruit
        draw_fruit(fruitX, fruitY)

        # eating fruit
        if eating(snakeX, fruitX, snakeY, fruitY):
            snake_length += 1
            fruitX = random.randrange(1, (WIDTH/10) - 1) * 10
            fruitY = random.randrange(1, (HIGHT/10) - 1) * 10

        snake_head = [snakeX, snakeY]
        pos.append(snake_head)
        if len(pos) > snake_length:
            del pos[0]

        score(white, 'roman', 20, snake_length * 10)
        draw_fruit(fruitX, fruitY)
        inc_size()

        for x in pos[:-1]:
            if x == snake_head:
                game_over = True
                pygame.time.delay(1)
                pos.clear()
        pygame.display.update()

        while game_over:
            screen.fill(green)
            over_text = (pygame.font.SysFont('roman', 50))
            text_surface = over_text.render('Game Over', True, white)
            screen.blit(text_surface, (220, 100))
            text_surface = over_text.render('press p to play again or esc to quit', True, white)
            screen.blit(text_surface, (5, 190))
            pygame.display.update()
            time.sleep(1)
            for event in pygame.event.get():
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_p:
                        game_over = False
                        snake()
                    elif event.key == pygame.K_ESCAPE:
                        quit()
                if event.type == pygame.QUIT:
                    running = False
                    game_over = False

        pygame.display.flip()

        clock.tick(60)

        pygame.display.update()

    quit()


def score(color, font, size, fruits):
    """
    function to show score on screen
    :return: none
    """
    # score system
    score_font = pygame.font.SysFont(font, size)
    score_surface = score_font.render('Score : ' + str(fruits - 10), True, color)
    score_rect = score_surface.get_rect()
    game_window.blit(score_surface, score_rect)


def draw_fruit(x, y):
    """
    :param x: x location on screen
    :param y: y location on screen
    :return: none
    """
    fruit = pygame.Rect(x, y, FRUIT_W, FRUIT_W)
    pygame.draw.rect(screen, white, fruit)


def inc_size():
    """
    increases size of snake
    :return:
    """
    for x in pos:
        #help(pygame.draw.rect)
        snake_body = pygame.Rect(x[0], x[1], BODY_W, BODY_W)
        pygame.draw.rect(screen, red, snake_body, 0, 10)


def eating(x1, x2, y1, y2):
    """
    calculate if snake ate fruit
    :param x1: x of snake
    :param x2: x of fruit
    :param y1: y of snake
    :param y2: y of fruit
    :return: true/false
    """
    d = math.sqrt(pow(x1 - x2, 2) + pow(y1 - y2, 2))
    if d < 15:
        return True
    else:
        return False


def main():
    snake()


if __name__ == "__main__":
    main()
    snake()
